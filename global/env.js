'use strict'

function convertEnvToEnum(e){
	if (e === "grass")
		return EnvEnum.GRASS;
	else if (e === "forest")
		return EnvEnum.FOREST;
	else if (e === "mountain")
		return EnvEnum.MOUNTAIN;
	else
		return -1;
}

function convertEnumToEnv(e){
	switch(e){
	case EnvEnum.GRASS:
		return "grass";
	case EnvEnum.FOREST:
		return "forest";
	case EnvEnum.MOUNTAIN:
		return "mountain";
	case EnvEnum.HQ_RED:
		return "HQred";
	case EnvEnum.HQ_BLUE:
		return "HQblue";
	default:
		return "";
	}
}

function randomEnvType(){
	// seed between 0 and 9
	var seed = Math.floor(Math.random()*100);
	
	if (0 <= seed && seed < 60)
		return "grass";
	else if (60 <= seed && seed < 85)
		return "forest";
	else
		return "mountain";
}

// Initialize envGrid array
var envGrid = [];
for(var x=0; x < GRID_WIDTH; x++){
	var col = [];
	for (var y=0; y < GRID_HEIGHT; y++)
		col.push(-1);
	envGrid.push(col);
}

// Initialize actorGrid array
var actorGrid = [];
	for(var x=0; x < GRID_WIDTH; x++){
	var col = [];
		for (var y=0; y < GRID_HEIGHT; y++)
		col.push({type:"", color:""});
	actorGrid.push(col);
}

// Grid Accessor functions
function getTile(x, y){
	return {env: envGrid[x][y], actor: actorGrid[x][y]};
}