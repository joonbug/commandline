// fill in grid with random environment + update envGrid
function randomGrid(){
	var grid = document.getElementById("grid");
	for (var y=0; y < GRID_HEIGHT; y++){
		var row = document.createElement("tr");
		for (var x=0; x < GRID_WIDTH; x++){
			var td = document.createElement("td");
			var envType = randomEnvType();
			td.className = envType;
			envGrid[x][y] = convertEnvToEnum(envType);
			td.id = "grid_"+y+"_"+x;
			row.appendChild(td);
		}
		grid.appendChild(row);
	}	
}

function assignEnvTile(y, x, env){
	document.getElementById("grid_"+y+"_"+x).className = convertEnumToEnv(env);
	envGrid[x][y] = env;
}

function AvvariaGrid(){
	// initialize grid to all grass
	var grid = document.getElementById("grid");
	for (var y=0; y < GRID_HEIGHT; y++){
		var row = document.createElement("tr");
		for (var x=0; x < GRID_WIDTH; x++){
			var td = document.createElement("td");
			var envType = EnvEnum.GRASS;
			td.className = convertEnumToEnv(envType);
			envGrid[x][y] = envType;
			td.id = "grid_"+y+"_"+x;
			row.appendChild(td);
		}
		grid.appendChild(row);
	}
	
	// set up map according to excel sheet
	if (true){ // assign MOUNTAIN terrain
		assignEnvTile(0,14, EnvEnum.MOUNTAIN);
		assignEnvTile(0,15, EnvEnum.MOUNTAIN);
		assignEnvTile(0,16, EnvEnum.MOUNTAIN);
		assignEnvTile(0,17, EnvEnum.MOUNTAIN);
		assignEnvTile(0,18, EnvEnum.MOUNTAIN);
		assignEnvTile(0,19, EnvEnum.MOUNTAIN);
		assignEnvTile(1,5, EnvEnum.MOUNTAIN);
		assignEnvTile(1,6, EnvEnum.MOUNTAIN);
		assignEnvTile(1,8, EnvEnum.MOUNTAIN);
		assignEnvTile(1,17, EnvEnum.MOUNTAIN);
		assignEnvTile(1,18, EnvEnum.MOUNTAIN);
		assignEnvTile(1,19, EnvEnum.MOUNTAIN);
		assignEnvTile(2,7, EnvEnum.MOUNTAIN);
		assignEnvTile(2,14, EnvEnum.MOUNTAIN);
		assignEnvTile(2,19, EnvEnum.MOUNTAIN);
		assignEnvTile(3,3, EnvEnum.MOUNTAIN);
		assignEnvTile(3,8, EnvEnum.MOUNTAIN);
		assignEnvTile(3,13, EnvEnum.MOUNTAIN);
		assignEnvTile(4,3, EnvEnum.MOUNTAIN);
		assignEnvTile(4,8, EnvEnum.MOUNTAIN);
		assignEnvTile(4,9, EnvEnum.MOUNTAIN);
		assignEnvTile(6,4, EnvEnum.MOUNTAIN);
		assignEnvTile(6,13, EnvEnum.MOUNTAIN);
		assignEnvTile(7,18, EnvEnum.MOUNTAIN);
		assignEnvTile(8,8, EnvEnum.MOUNTAIN);
		assignEnvTile(8,11, EnvEnum.MOUNTAIN);
		assignEnvTile(8,17, EnvEnum.MOUNTAIN);
		assignEnvTile(9,7, EnvEnum.MOUNTAIN);
		assignEnvTile(9,11, EnvEnum.MOUNTAIN);
		assignEnvTile(9,17, EnvEnum.MOUNTAIN);
	}
	if (true){ // assign FOREST terrain
		assignEnvTile(0,6, EnvEnum.FOREST);
		assignEnvTile(0,7, EnvEnum.FOREST);
		assignEnvTile(1,3, EnvEnum.FOREST);
		assignEnvTile(1,4, EnvEnum.FOREST);
		assignEnvTile(1,7, EnvEnum.FOREST);
		assignEnvTile(2,3, EnvEnum.FOREST);
		assignEnvTile(2,8, EnvEnum.FOREST);
		assignEnvTile(3,10, EnvEnum.FOREST);
		assignEnvTile(3,14, EnvEnum.FOREST);
		assignEnvTile(3,19, EnvEnum.FOREST);
		assignEnvTile(4,2, EnvEnum.FOREST);
		assignEnvTile(4,6, EnvEnum.FOREST);
		assignEnvTile(4,12, EnvEnum.FOREST);
		assignEnvTile(5,2, EnvEnum.FOREST);
		assignEnvTile(5,6, EnvEnum.FOREST);
		assignEnvTile(5,12, EnvEnum.FOREST);
		assignEnvTile(6,8, EnvEnum.FOREST);
		assignEnvTile(6,9, EnvEnum.FOREST);
		assignEnvTile(7,7, EnvEnum.FOREST);		
		assignEnvTile(7,13, EnvEnum.FOREST);
		assignEnvTile(8,0, EnvEnum.FOREST);
		assignEnvTile(8,3, EnvEnum.FOREST);
		assignEnvTile(8,9, EnvEnum.FOREST);
		assignEnvTile(8,10, EnvEnum.FOREST);
		assignEnvTile(8,14, EnvEnum.FOREST);
		assignEnvTile(8,16, EnvEnum.FOREST);
		assignEnvTile(9,0, EnvEnum.FOREST);
		assignEnvTile(9,8, EnvEnum.FOREST);
		assignEnvTile(9,9, EnvEnum.FOREST);
		assignEnvTile(9,10, EnvEnum.FOREST);
		assignEnvTile(9,16, EnvEnum.FOREST);
	}
	if (true){ // assign OTHER terrain
		assignEnvTile(4,0, EnvEnum.HQ_RED);
		assignEnvTile(5,19, EnvEnum.HQ_BLUE);
	}
}


// for (var x=0; x < GRID_WIDTH; x++)
	// for (var y=0; y < GRID_HEIGHT; y++)
		// console.log(getTile(x, y));
AvvariaGrid();
fillGrid();