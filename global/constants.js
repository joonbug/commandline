'use strict'

const GRID_WIDTH = 20;
const GRID_HEIGHT = 10;
const NUM_ACTIONS = 8;
const NUM_UNIT_TYPES = 3;


var EnvEnum = {
	GRASS: 0,
	FOREST: 1,
	MOUNTAIN: 2,
	HQ_RED: 3,
	HQ_BLUE: 4
};

var DirEnum = {
	NORTH: 0,
	WEST: 1,
	SOUTH: 2,
	EAST: 3
};

var DirString = ["North", "West", "South", "East"];

var ActionEnum = {
	NOTHING: -1,
	FORWARD: 0,
	BACKWARD: 1,
	LEFT: 2,
	RIGHT: 3,
	ATTACK: 4,
	HOLD: 5, // AS IN HOLD POSITION
	HIDE: 6 // ONLY IN FOREST
};

var UnitEnum = {
	INFANTRY: 0,
	TANK: 1,
	ARTILLERY: 2
};

var UnitString = ["Infantry", "Tank", "Artillery"];
var UnitDisplay = ['@', 'TK', '%'];

var StateEnum = {
	MOBILE: 0,
	STATIONARY: 1
};