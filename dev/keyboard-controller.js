'use strict'

document.addEventListener('keydown', function(event){
	if (event.target.tagName.toLowerCase() !== 'textarea'){
		switch(event.keyCode){
		case 37:
			test.attemptAction(ActionEnum.LEFT);
			fillDebugger(test.dir, test.ammo, "");
			break;
		case 38:
			test.attemptAction(ActionEnum.FORWARD);
			break;
		case 39:
			test.attemptAction(ActionEnum.RIGHT);
			fillDebugger(test.dir, test.ammo, "");
			break;
		case 40:
			test.attemptAction(ActionEnum.BACKWARD);
			break;
		case 65: // a
			test.attemptAction(ActionEnum.ATTACK);
			break;
		case 68: // d
			test.attemptAction(ActionEnum.HOLD);
				break;
		case 72: // h
			test.attemptAction(ActionEnum.HIDE);
				break;
		default:
			break;
		}
	}
	
	fillGrid();
});