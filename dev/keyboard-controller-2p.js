document.addEventListener('keydown', function(event){
	switch(event.keyCode){
	case 37:
		moveActor(test2, MoveEnum.LEFT);
		break;
	case 38:
		moveActor(test2, MoveEnum.UP);
		break;
	case 39:
		moveActor(test2, MoveEnum.RIGHT);
		break;
	case 40:
		moveActor(test2, MoveEnum.DOWN);
		break;
	case 65:
		moveActor(test, MoveEnum.LEFT);
		break;
	case 87:
		moveActor(test, MoveEnum.UP);
		break;
	case 68:
		moveActor(test, MoveEnum.RIGHT);
		break;
	case 83:
		moveActor(test, MoveEnum.DOWN);
		break;
	default:
		break;
	}
});