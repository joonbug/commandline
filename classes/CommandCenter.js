'use strict'

/******************************************************************************

CommandCenter class

On pressing 'Update Orders' button, CommandCenter processes the corresponding
Command Tree into an array of functions that return ActionEnum

CommandCenter Properties:
	function[#UnitType] commands
		commands[i] => function[]
	string[#UnitType] rawCommands
	
ACCEPTED TRIGGERS:
	WHEN ON [ENVIRONMENT]			
	WHEN ENEMY SIGHTED				
	WHEN ALWAYS						
ACCEPTED COMMANDS:
	ATTACK (ENEMY SIGHTED ONLY)
	HOLD (ENEMY SIGHTED ONLY)
	HIDE (FOREST ONLY)
	MOVE FORWARD/BACKWARD
	TURN LEFT/RIGHT
	DO NOTHING
ACCEPTED UNLESSES:
	UNLESS ENEMY IS [TYPE]
	UNLESS ON [ENVIRONMENT]
	UNLESS OUT OF AMMO
	
******************************************************************************/


var CommandCenter = function(){
	this.commands = [];
	this.rawCommands = [];
	this.currentUnit = -1;
	for (var i=0; i < NUM_UNIT_TYPES; i++){
		this.commands.push([(function(actor){return ActionEnum.NOTHING;})]);
		this.rawCommands.push("");
	}
	
	/* TEST PURPOSE DEFAULT COMMANDS */
	this.rawCommands[UnitEnum.INFANTRY] = 
		"WHEN ENEMY SIGHTED\n"+
		"MOVE BACKWARD\n"+
		"UNLESS ENEMY IS INFANTRY\n"+
		"DO ATTACK\n"+
		"UNLESS OUT OF AMMO\n"+
		"DO HOLD\n\n"+
		"WHEN ALWAYS DO MOVE FORWARD";
	this.rawCommands[UnitEnum.TANK] = "WHEN ALWAYS\nDO HOLD";
	this.rawCommands[UnitEnum.ARTILLERY] = "WHEN ALWAYS\nDO HOLD"
	/**/
};

var Command = function(tokens){
	this.tokens = tokens;
	this.trigger = [];	// string[]
	this.command = [];	// string[]
	this.unless = null;	// Command
};

CommandCenter.prototype = Object.create(Object.prototype);
Command.prototype = Object.create(Object.prototype);

// UnitType UI Button Handling
CommandCenter.prototype.getRawCommands = function(unitType){
	this.rawCommands[this.currentUnit] = document.getElementById("command-center").value;
	document.getElementById("command-center").value = this.rawCommands[unitType];
	this.currentUnit = unitType;
}

// *** TODO: Enable recursive UNLESS and multiple UNLESS ***
// parses token string into command tree
// @index - Current index of placement in tokens
// @level - Current level of recursion
Command.prototype.parseCommands = function(index, level){
	// create Command class tree
	var cmdSeqPos = 0; // how far along the format the tokens are
	var curUnless = null;
	var tokens = this.tokens;
	
	for (var i=0; i < tokens.length; i++){
		if (tokens[i] === 'DO' && cmdSeqPos === 0){
			cmdSeqPos = 1;
		} else if (tokens[i] === 'UNLESS' && cmdSeqPos === 1){
			this.unless = new Command();
			curUnless = this.unless;
			cmdSeqPos = 2;
		} else if (tokens[i] === 'UNLESS' && cmdSeqPos === 3){
			curUnless.unless = new Command();
			curUnless = curUnless.unless;
			cmdSeqPos = 2;
		} else if (tokens[i] === 'DO' && cmdSeqPos === 2){
			cmdSeqPos = 3;
		// } else if (tokens[i] === 'UNLESS' && cmdSeqPos === 3){
			// TODO: Count number of \t to match with curLevel
			
			// if (tokens[i-1] === '\t'){ // next level of unlesses
			
			// } else{
				// numUnless++;
			// }
		// }
		} else if (tokens[i] === '\n' || tokens[i] === '\t'){
			continue;
		} else if (tokens[i] !== 'DO' && tokens[i] !== 'UNLESS'){
			switch(cmdSeqPos){
			case 0: // trigger token
				this.trigger.push(tokens[i]);
				break;
			case 1: // command token
				this.command.push(tokens[i]);
				break;
			case 2: // unless trigger
				curUnless.trigger.push(tokens[i]);
				break;
			case 3: // unless command
				curUnless.command.push(tokens[i]);
				break;
			default:
				console.log('Invalid command sequence position');
			}
		} else{
			console.log("Invalid token string format");
		}
			
	}
}

/******************************************************************************
ACCEPTED TRIGGERS:
	WHEN ON [ENVIRONMENT]			
	WHEN NOT ON [ENVIRONMENT]
	WHEN ENEMY SIGHTED				
	WHEN NO ENEMY SIGHTED
	WHEN ALWAYS
ACCEPTED COMMANDS:
	ATTACK (ENEMY SIGHTED ONLY)
	DEFEND (ENEMY SIGHTED ONLY)
	HIDE (FOREST ONLY)
	MOVE FORWARD/BACKWARD
	TURN LEFT/RIGHT
	DO NOTHING
ACCEPTED UNLESSES:
	UNLESS ENEMY IS [TYPE]
	UNLESS ON [ENVIRONMENT]
	UNLESS OUT OF AMMO
******************************************************************************/

Command.prototype.convertUnlessToIf = function(){
	var functionStr = "";
	
	// helper functions
	function isEnvironment(str){
		return (str === 'PLAIN' || str === 'FOREST' || str === 'MOUNTAIN');
	}
	
	function isUnit(str){
		return (str === 'INFANTRY' || str === 'TANK' || str === 'ARTILLERY');
	}
	
	// Convert secondary trigger
	switch(this.trigger[0]){
	case 'ON':
		if (isEnvironment(this.trigger[1]))
			functionStr += "if(getTile(actor.xpos, actor.ypos).env === EnvEnum."+this.trigger[1]+"){ ";
		else{
			console.log("Trigger ON requires an environment type");
			return "";
		}
		break;
	case 'NOT':
		if (this.trigger[1] === 'ON' && isEnvironment(this.trigger[2]))
			functionStr += "if(getTile(actor.xpos, actor.ypos).env !== EnvEnum."+this.trigger[2]+"){ ";
		break;
	case 'ENEMY':
		if (this.trigger[1] === 'IS' && isUnit(this.trigger[2]))
			functionStr += "if(actor.search(actor.detect(), UnitEnum."+this.trigger[2]+")){ ";
		break;
	case 'OUT':
		if (this.trigger[1] === 'OF' && this.trigger[2] === 'AMMO')
			functionStr += "if(actor.ammo === 0){ ";
		break;
	default:
		console.log("Invalid trigger array:");
		console.log(this.trigger);
		return "";
	}
	
	// Convert unless
	if (this.unless !== null){
		var unlessStr = this.unless.convertUnlessToIf();
		//console.log("Unless string:");
		//console.log(unlessStr);
		functionStr += unlessStr;
	}
	
	// Convert command
	switch(this.command[0]){
	case 'ATTACK':
	case 'HOLD':
	case 'HIDE':
	case 'NOTHING':
		functionStr += "return ActionEnum."+this.command[0]+";}";
		break;
	case 'MOVE':
		if (this.command[1] !== 'FORWARD' && this.command[1] !== 'BACKWARD'){
			console.log("Unit can only move forward or backward");
			return "";
		}
		functionStr += "return ActionEnum."+this.command[1]+";}";
		break;
	case 'TURN':
		if (this.command[1] !== 'LEFT' && this.command[1] !== 'RIGHT'){
			console.log("Unit can only turn left or right");
			return "";
		}
		functionStr += "return ActionEnum."+this.command[1]+";}";
		break;
	default:
		console.log("Unknown command given");
		return "";
	}
	
	return functionStr;
};

Command.prototype.convertToFunction = function(){
	var functionStr = "(function(actor){";
	
	// helper functions
	function isEnvironment(str){
		return (str === 'PLAIN' || str === 'FOREST' || str === 'MOUNTAIN');
	}
	
	function isUnit(str){
		return (str === 'INFANTRY' || str === 'TANK' || str === 'ARTILLERY');
	}
	
	// Convert trigger
	switch(this.trigger[1]){
	case 'ALWAYS':
		functionStr += "if(true){ ";
		break;
	case 'ON':
		if (isEnvironment(this.trigger[2]))
			functionStr += "if(getTile(actor.xpos, actor.ypos).env === EnvEnum."+this.trigger[2]+"){ ";
		else{
			console.log("Trigger ON requires an environment type");
			return "";
		}
		break;
	case 'NOT':
		if (this.trigger[2] === 'ON' && isEnvironment(this.trigger[3]))
			functionStr += "if(getTile(actor.xpos, actor.ypos).env !== EnvEnum."+this.trigger[3]+"){ ";				
	case 'ENEMY':
		if (this.trigger[2] === 'SIGHTED')
			functionStr += "if(actor.detect().length > 0){ ";
		break;
	case 'NO':
		if (this.trigger[2] === 'ENEMY' && this.trigger[3] === 'SIGHTED')
			functionStr += "if(actor.detect().length === 0){ ";
		break;
	default:
		console.log("Invalid trigger array:");
		console.log(this.trigger);
		return "";
	}
	
	// Convert unless
	if (this.unless !== null){
		var unlessStr = this.unless.convertUnlessToIf(); 
		//console.log("Unless string:");
		//console.log(unlessStr);
		functionStr += unlessStr;
	}
	
	// Convert command
	switch(this.command[0]){
	case 'ATTACK':
	case 'HOLD':
	case 'HIDE':
	case 'NOTHING':
		functionStr += "return ActionEnum."+this.command[0]+";}";
		break;
	case 'MOVE':
		if (this.command[1] !== 'FORWARD' && this.command[1] !== 'BACKWARD'){
			console.log("Unit can only move forward or backward");
			return "";
		}
		functionStr += "return ActionEnum."+this.command[1]+";}";
		break;
	case 'TURN':
		if (this.command[1] !== 'LEFT' && this.command[1] !== 'RIGHT'){
			console.log("Unit can only turn left or right");
			return "";
		}
		functionStr += "return ActionEnum."+this.command[1]+";}";
		break;
	default:
		console.log("Unknown command given");
		return "";
	}
	
	functionStr += "})";
	return functionStr;
}

CommandCenter.prototype.updateCommands = function(/* UnitEnum */ unitType, tokens){
	var command = [];
	
	// default DEFAULT ACTION is unit[0]
	command.push((function(actor){ return ActionEnum.NOTHING}));
	
	// split tokens array and create Command array containing each
	var cmdArray = [];
	var numCommands = 0;
	for (var i=0; i < tokens.length; i++){
		if (tokens[i] === 'WHEN'){
			cmdArray.push(new Command([tokens[i]]));
			numCommands++;
		} else
			cmdArray[numCommands-1].tokens.push(tokens[i]);
	}
	
	// parse token arrays into command trees
	for (var i=0; i < numCommands; i++){
		cmdArray[i].parseCommands(0, 0);
		console.log(cmdArray[i]);
		var func = cmdArray[i].convertToFunction();
		//console.log(func);
		if (cmdArray[i].trigger[1] === 'ALWAYS')
			command[0] = eval(func);
		else
			command.push(eval(func));
	}

	this.commands[unitType] = command;
};

CommandCenter.prototype.process = function(unitType){
	var commandString = document.getElementById("command-center").value;
	commandString = commandString.toUpperCase();
	
	var commandTokens = commandString.match(/[A-Z]+|\n|\t/g);
	
	this.updateCommands(unitType, commandTokens);
	
	console.log(this.commands);
};