'use strict'

/******************************************************************************
Artillery class

Subclass of Actor class
	
Artillery Properties:
	INT		  xpos, ypos  => coordinates on Grid
	UnitEnum  type 		  => subclass of Actor
	String    color  	  => allegiance (red v blue)
	EnvEnum   env 		  => environment of Actor's current Grid tile 
	StateEnum state 	  => mobile, stationary
	INT       ammo		  => ammount of attacks available
	INT		  maxAmmo	  => maximum ammo for unit
	DirEnum	  dir		  => direction unit is currently facing
	Boolean   hidden	  => whether unit is hidden in a forest or not
		
Special Properties of Artillery
	ARTILLERY:
		Vision range - 3
		Can Attack Actors 2 tiles away
		5 AMMO

******************************************************************************/

var Artillery = function(xpos, ypos, color, dir){
	this.xpos = xpos;
	this.ypos = ypos;
	this.type = UnitEnum.ARTILLERY;
	this.color = color;
	this.state = -1;
	this.ammo = 5;
	this.maxAmmo = 5;
	this.dir = dir;
	this.hidden = false;
	
	this.updateGrid(xpos, ypos);
};

Artillery.prototype = Object.create(Actor.prototype);

// for ENEMY SIGHTED trigger
Tank.prototype.detect = function(){
	// create Sight range
	var sightRange = [];
	
	// valid Grid tile boolean function
	function pushIfValidTile(xpos, ypos){
		if(xpos >= 0 && xpos < GRID_WIDTH && ypos >= 0 && ypos < GRID_HEIGHT)
			sightRange.push({x: xpos, y: ypos});
	}
	
	switch(this.dir){
	case DirEnum.NORTH:
		pushIfValidTile(this.xpos+0, this.ypos-3);

		pushIfValidTile(this.xpos-1, this.ypos-2);
		pushIfValidTile(this.xpos+0, this.ypos-2);
		pushIfValidTile(this.xpos+1, this.ypos-2);

		pushIfValidTile(this.xpos-2, this.ypos-1);
		pushIfValidTile(this.xpos-1, this.ypos-1);
		pushIfValidTile(this.xpos+0, this.ypos-1);
		pushIfValidTile(this.xpos+1, this.ypos-1);
		pushIfValidTile(this.xpos+2, this.ypos-1);
		break;
	case DirEnum.SOUTH:
		pushIfValidTile(this.xpos+0, this.ypos+3);

		pushIfValidTile(this.xpos-1, this.ypos+2);
		pushIfValidTile(this.xpos+0, this.ypos+2);
		pushIfValidTile(this.xpos+1, this.ypos+2);

		pushIfValidTile(this.xpos-2, this.ypos+1);
		pushIfValidTile(this.xpos-1, this.ypos+1);
		pushIfValidTile(this.xpos+0, this.ypos+1);
		pushIfValidTile(this.xpos+1, this.ypos+1);
		pushIfValidTile(this.xpos+2, this.ypos+1);
		break;
	case DirEnum.WEST:
		pushIfValidTile(this.xpos-3, this.ypos+0);

		pushIfValidTile(this.xpos-2, this.ypos-1);
		pushIfValidTile(this.xpos-2, this.ypos+0);
		pushIfValidTile(this.xpos-2, this.ypos+1);

		pushIfValidTile(this.xpos-1, this.ypos-2);
		pushIfValidTile(this.xpos-1, this.ypos-1);
		pushIfValidTile(this.xpos-1, this.ypos+0);
		pushIfValidTile(this.xpos-1, this.ypos+1);
		pushIfValidTile(this.xpos-1, this.ypos+2);
		break;
	case DirEnum.EAST:
		pushIfValidTile(this.xpos+3, this.ypos+0);

		pushIfValidTile(this.xpos+2, this.ypos-1);
		pushIfValidTile(this.xpos+2, this.ypos+0);
		pushIfValidTile(this.xpos+2, this.ypos+1);

		pushIfValidTile(this.xpos+1, this.ypos-2);
		pushIfValidTile(this.xpos+1, this.ypos-1);
		pushIfValidTile(this.xpos+1, this.ypos+0);
		pushIfValidTile(this.xpos+1, this.ypos+1);
		pushIfValidTile(this.xpos+1, this.ypos+2);
		break;
	default:
		console.log("Invalid actor direction");
		return [];
	}
	
	var targets = [];
	for (var i=0; i < sightRange.length; i++){
		if (getTile(sightRange[i].x, sightRange[i].y)["actor"].type !== ""){
			var gridTile = document.getElementById("grid_"+sightRange[i].y+"_"+sightRange[i].x);
			if (gridTile.lastChild !== null && gridTile.lastChild.id !== this.color)
				targets.push(sightRange[i]);
		}
	}
	
	// attack will prioritize Tank > Artillery > Infantry
	return targets;
};