'use strict'

/******************************************************************************
Infantry class

Subclass of Actor class
	
Actor Properties:
	INT		  xpos, ypos  => coordinates on Grid
	UnitEnum  type 		  => subclass of Actor
	String    color  	  => allegiance (red v blue)
	EnvEnum   env 		  => environment of Actor's current Grid tile 
	StateEnum state 	  => mobile, stationary
	INT       ammo		  => ammount of attacks available
	INT		  maxAmmo	  => maximum ammo for unit
	DirEnum	  dir		  => direction unit is currently facing
	Boolean   hidden	  => whether unit is hidden in a forest or not
	INT		  vision	  => how far the unit can detect enemies from
	
Special Properties of Infantry
	INFANTRY:
		Vision range - 1
		Unaffected by Terrain
		2 Ammo

******************************************************************************/

var Infantry = function(xpos, ypos, color, dir){
	this.xpos = xpos;
	this.ypos = ypos;
	this.type = UnitEnum.INFANTRY;
	this.color = color;
	this.state = -1;
	this.ammo = 2;
	this.maxAmmo = 2;
	this.dir = dir;
	this.hidden = false;
	
	this.updateGrid(xpos, ypos);
};

Infantry.prototype = Object.create(Actor.prototype);

Infantry.prototype.detect = function(){
	// create Sight range
	var sightRange = [];
	
	// valid Grid tile boolean function
	function pushIfValidTile(xpos, ypos){
		if(xpos >= 0 && xpos < GRID_WIDTH && ypos >= 0 && ypos < GRID_HEIGHT)
			sightRange.push({x: xpos, y: ypos});
	}
	
	switch(this.dir){
	case DirEnum.NORTH:
		pushIfValidTile(this.xpos, this.ypos-1);
		break;
	case DirEnum.SOUTH:
		pushIfValidTile(this.xpos, this.ypos+1);
		break;
	case DirEnum.WEST:
		pushIfValidTile(this.xpos-1, this.ypos);
		break;
	case DirEnum.EAST:
		pushIfValidTile(this.xpos+1, this.ypos);
		break;
	default:
		console.log("Invalid actor direction");
		return [];
	}
	
	if (sightRange.length > 0)
		if (getTile(sightRange[0].x, sightRange[0].y)["actor"].type !== ""){
			var gridTile = document.getElementById("grid_"+sightRange[0].y+"_"+sightRange[0].x);
			if (gridTile.lastChild !== null && gridTile.lastChild.id !== this.color)
				return sightRange;
		}
	
	return [];
};