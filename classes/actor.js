'use strict'

/******************************************************************************
Actor class

Superclass for all Units (i.e. Infantry, Tank, or Artillery)
	
Actor Properties:
	INT		  xpos, ypos  => coordinates on Grid
	UnitEnum  type 		  => subclass of Actor
	String    color  	  => allegiance (red v blue)
	EnvEnum   env 		  => environment of Actor's current Grid tile 
	StateEnum state 	  => mobile, stationary
	INT       ammo		  => ammount of attacks available
	INT		  maxAmmo	  => maximum ammo for unit
	DirEnum	  dir		  => direction unit is currently facing
	Boolean   hidden	  => whether unit is hidden in a forest or not
	INT		  vision	  => how far the unit can detect enemies from
	
ACTOR ACTION OPTIONS ARE AS FOLLOWS
	1. Move Forward
	1b. Turning direction on panel is 1 separate movement
	2. Attack in current direction
	3. Defend in current direction
	4. Hide (if in Forest)
	
Special Properties of Different Actors
	INFANTRY:
		Vision range - 2
		Unaffected by Terrain
	TANK:
		Vision range - 2
		Attack ALL Enemies in Sight range
		1 AMMO
	ARTILLERY:
		Vision range - 3
		Can Attack Actors 2 tiles away
		5 AMMO

******************************************************************************/

var Actor = function(xpos, ypos, type, color, dir){
	this.xpos = xpos;
	this.ypos = ypos;
	this.type = type;
	this.color = color;
	this.state = -1;
	this.ammo = 1;
	this.maxAmmo = 1;
	this.dir = dir;
	this.hidden = false;
	
	this.updateGrid(xpos, ypos);
};
Actor.prototype = Object.create(Object.prototype);

Actor.prototype.validToMoveTo = function(xpos, ypos){
	if (GRID_WIDTH < xpos || xpos < 0)
		return false;
	if (GRID_HEIGHT < ypos || ypos < 0)
		return false;
	if (envGrid[xpos][ypos] === EnvEnum.MOUNTAIN && this.type !== UnitEnum.INFANTRY)
		return false;
	return actorGrid[xpos][ypos].type === "";
};

Actor.prototype.updateGrid = function(newX, newY){
	// reset previous Actor tile coordinates
	actorGrid[this.xpos][this.ypos].type = "";
	actorGrid[this.xpos][this.ypos].color = "";
	
	var unitType = "";
	
	switch(this.type){
	case UnitEnum.INFANTRY:
		unitType = "@";
		break;
	case UnitEnum.TANK:
		unitType = "TK";
		break;
	case UnitEnum.ARTILLERY:
		unitType = "%";
		break;
	default:
		console.log("Invalid UnitType "+this.type+" call to tile ("+newX+","+newY+")");
	}

	// set new Actor tile attributes
	if (!this.hidden)
		actorGrid[newX][newY].type = unitType;
	actorGrid[newX][newY].color = this.color;
}

// Update position if move acceptable
Actor.prototype.updatePos = function(xpos, ypos){
	this.hidden = false; // Hidden status removed when moving
	this.updateGrid(xpos, ypos);
	this.xpos = xpos;
	this.ypos = ypos;
}

Actor.prototype.turn = function(left){
	if (left){ // turn left
		if (this.dir !== DirEnum.EAST)
			this.dir++; 
		else
			this.dir = DirEnum.NORTH;
	}
	else{ // turn right
		if (this.dir !== DirEnum.NORTH)
			this.dir--;
		else
			this.dir = DirEnum.EAST;
	}
}

// for ENEMY SIGHTED trigger
// *** ONLY SUPER FUNCTION ***
Actor.prototype.detect = function(){
	// create Sight range
	var sightRange = [];
	console.log("Unspecified Actor type - No enemy sighted");
	return sightRange;
};

// for UNLESS ENEMY IS trigger
Actor.prototype.search = function(targets, type){
	for (var i=0; i < targets.length; i++){
		if (UnitDisplay[type] === getTile(targets[i].x, targets[i].y).actor.type)
			return true;
	}
	return false;
}

Actor.prototype.hide = function(){
	this.hidden = true;
	this.updateGrid(this.xpos, this.ypos);
}

// Actors will attempt move from Actor#1 to Actor#N
Actor.prototype.attemptMove = function(dir){
	switch(dir){
	case DirEnum.NORTH:
		if (this.ypos-1 >= 0 && this.validToMoveTo(this.xpos, this.ypos-1)){
			this.updatePos(this.xpos, this.ypos-1);
			fillDebugger(test.dir, test.ammo, 
				this.color+" "+UnitString[this.type]+" moved "+DirString[dir]+" to ["+this.xpos+","+this.ypos+"]");
		}
		break;
	case DirEnum.SOUTH:
		if (this.ypos+1 < GRID_HEIGHT && this.validToMoveTo(this.xpos, this.ypos+1)){
			this.updatePos(this.xpos, this.ypos+1);
			fillDebugger(test.dir, test.ammo, 
				this.color+" "+UnitString[this.type]+" moved "+DirString[dir]+" to ["+this.xpos+","+this.ypos+"]");
		}
		break;
	case DirEnum.WEST:
		if (this.xpos-1 >= 0 && this.validToMoveTo(this.xpos-1, this.ypos)){
			this.updatePos(this.xpos-1, this.ypos);
			fillDebugger(test.dir, test.ammo, 
				this.color+" "+UnitString[this.type]+" moved "+DirString[dir]+" to ["+this.xpos+","+this.ypos+"]");
		}
		break;
	case DirEnum.EAST:
		if (this.xpos+1 < GRID_WIDTH && this.validToMoveTo(this.xpos+1, this.ypos)){
			this.updatePos(this.xpos+1, this.ypos);
			fillDebugger(test.dir, test.ammo, 
				this.color+" "+UnitString[this.type]+" moved "+DirString[dir]+" to ["+this.xpos+","+this.ypos+"]");
		}
		break;
	default:
		console.log("Invalid move request. Actor stands still.");
		return false;
	}
	return true;
};

Actor.prototype.attemptAction = function(action){
	switch(action){
	case ActionEnum.NOTHING:
		break;
	case ActionEnum.FORWARD:
		this.attemptMove(this.dir);
		break;
	case ActionEnum.BACKWARD:
		this.attemptMove((this.dir+2)%4); // opposite dir
		break;
	case ActionEnum.LEFT:
		this.turn(true); // turn left
		this.attemptMove(this.dir);
		break;
	case ActionEnum.RIGHT:
		this.turn(false); // turn right
		this.attemptMove(this.dir);
		break;
	case ActionEnum.ATTACK:
		if (this.ammo > 0){
			var target = this.detect();
			
			if (target.length > 0){
				this.ammo--;// TODO: Implement Attack		
				this.hidden = false;
				fillDebugger(test.dir, test.ammo, 
					this.color+" "+UnitString[this.type]+" fired "+DirString[this.dir]+" at ["+this.xpos+","+this.ypos+"]!!!");
			} else{
				fillDebugger(test.dir, test.ammo,
					this.color+" "+UnitString[this.type]+" had no one to attack "+DirString[this.dir]+" of ["+this.xpos+","+this.ypos+"]");				
			}
		} else{
			fillDebugger(test.dir, test.ammo, 
				this.color+" "+UnitString[this.type]+" has no ammo to attack "+DirString[this.dir]+" of ["+this.xpos+","+this.ypos+"]");
		}
		break;
	case ActionEnum.HOLD:
		if (this.ammo < this.maxAmmo)
			this.ammo = this.maxAmmo;
		fillDebugger(test.dir, test.ammo,
			this.color+" "+UnitString[this.type]+" reloading and holding ["+this.xpos+","+this.ypos+"]");
		break;
	case ActionEnum.HIDE:
		if (envGrid[this.xpos][this.ypos] === EnvEnum.FOREST){
			this.hide();
			fillDebugger(test.dir, test.ammo, 
				this.color+" "+UnitString[this.type]+" hiding at ["+this.xpos+","+this.ypos+"]...");
		} else{
			fillDebugger(test.dir, test.ammo, 
				this.color+" "+UnitString[this.type]+" cannot hide at ["+this.xpos+","+this.ypos+"]");			
		}
		break;
	default:
		console.log("Invalid Action request. Actor does nothing.");
	}
};

