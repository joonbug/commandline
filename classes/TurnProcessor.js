'use strict'

/******************************************************************************

TurnProcess class

Every tick, TurnProcessor goes through 1...n Actor object in actors array
and executes action depending on corresponding command of Actor type

TurnProcess Properties:
	Actor[] actors
	CommandCenter cc

******************************************************************************/


var TurnProcessor = function(actors){
	this.actors = actors;
	this.cc = new CommandCenter();
};

TurnProcessor.prototype = Object.create(Object.prototype);

TurnProcessor.prototype.chooseActorAction = function(actorIndex){	
	var actor = this.actors[actorIndex];
	var command = this.cc.commands[actor.type];

	for (var i=1; i < command.length; i++){
		console.log(command[i]);
		var action = command[i](actor);
		if (action !== undefined)
			return action;
	}
	
	return command[0](actor);
};

TurnProcessor.prototype.directActors = function(){
	//console.log(this);
	
	for (var i=0; i < this.actors.length; i++){
		var actorCommand = this.chooseActorAction(i);
		this.actors[i].attemptAction(actorCommand);
	}
};