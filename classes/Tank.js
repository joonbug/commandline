'use strict'

/******************************************************************************
Tank class

Subclass of Actor class
	
Actor Properties:
	INT		  xpos, ypos  => coordinates on Grid
	UnitEnum  type 		  => subclass of Actor
	String    color  	  => allegiance (red v blue)
	EnvEnum   env 		  => environment of Actor's current Grid tile 
	StateEnum state 	  => mobile, stationary
	INT       ammo		  => ammount of attacks available
	INT		  maxAmmo	  => maximum ammo for unit
	DirEnum	  dir		  => direction unit is currently facing
	Boolean   hidden	  => whether unit is hidden in a forest or not
	INT		  vision	  => how far the unit can detect enemies from
	
Special Properties of Tank
	TANK:
		Vision range - 2
		Attack ALL Enemies in Sight range
		1 AMMO

******************************************************************************/

var Tank = function(xpos, ypos, color, dir){
	this.xpos = xpos;
	this.ypos = ypos;
	this.type = UnitEnum.TANK;
	this.color = color;
	this.state = -1;
	this.ammo = 1;
	this.maxAmmo = 1;
	this.dir = dir;
	this.hidden = false;
	
	this.updateGrid(xpos, ypos);
};

Tank.prototype = Object.create(Actor.prototype);

// for ENEMY SIGHTED trigger
Tank.prototype.detect = function(){
	// create Sight range
	var sightRange = [];
	
	// valid Grid tile boolean function
	function pushIfValidTile(xpos, ypos){
		if(xpos >= 0 && xpos < GRID_WIDTH && ypos >= 0 && ypos < GRID_HEIGHT)
			sightRange.push({x: xpos, y: ypos});
	}
	
	switch(this.dir){
	case DirEnum.NORTH:
		pushIfValidTile(this.xpos, this.ypos-2);
		pushIfValidTile(this.xpos-1, this.ypos-1);
		pushIfValidTile(this.xpos, this.ypos-1);
		pushIfValidTile(this.xpos+1, this.ypos-1);
		break;
	case DirEnum.SOUTH:
		pushIfValidTile(this.xpos, this.ypos+2);
		pushIfValidTile(this.xpos-1, this.ypos+1);
		pushIfValidTile(this.xpos, this.ypos+1);
		pushIfValidTile(this.xpos+1, this.ypos+1);
		break;
	case DirEnum.WEST:
		pushIfValidTile(this.xpos-2, this.ypos);
		pushIfValidTile(this.xpos-1, this.ypos-1);
		pushIfValidTile(this.xpos-1, this.ypos);
		pushIfValidTile(this.xpos-1, this.ypos+1);
		break;
	case DirEnum.EAST:
		pushIfValidTile(this.xpos+2, this.ypos);
		pushIfValidTile(this.xpos+1, this.ypos-1);
		pushIfValidTile(this.xpos+1, this.ypos);
		pushIfValidTile(this.xpos+1, this.ypos+1);
		break;
	default:
		console.log("Invalid actor direction");
		return [];
	}
	
	var targets = [];
	for (var i=0; i < sightRange.length; i++){
		if (getTile(sightRange[i].x, sightRange[i].y)["actor"].type !== ""){
			var gridTile = document.getElementById("grid_"+sightRange[i].y+"_"+sightRange[i].x);
			if (gridTile.lastChild !== null && gridTile.lastChild.id !== this.color)
				targets.push(sightRange[i]);
		}
	}
	
	return targets;
};